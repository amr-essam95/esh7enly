# Esh7enly

## Introduction

This Mobile application helps people charge credit for communication companies, it supports Etisalat, Mobinil and Vodafone cards.

## Team Members

* Amr Essam Mohamed Barakat (1300893)
* Mohamed Ossama Abu Elhassan (1301070)
* Mohamed Hatem Sayed Abd Elhamid (1301098)
* Ahmed Hawaf Abd Elhakim Talab (1300235)

## Technologies Used

This application uses Ionic for mobile development and Django REST framework for backend development.
In this application, the user uses the mobile to capture a card or even load an image of the card from gallery and then send the image to the 
backend to process the image and return the code of the credit card which will be copied to clipboard to make chargin cards easier.

## Implemetation

In this application, we used Image processing techniques mixed with KNNs to detect the numbers in the cards, we detect the rectangle that contains
the numbers and then use pytesseract OCR to get the numbers from the image.

The image processing algorithm is inspired from the plate detection in cars with major differences. The plate detector contains numbers and characters 
but our rectangle contains only numbers.

## Demonstration Video

Link: https://youtu.be/YVBaOClfvME

## Try the application

We you can try the application by simulation on your ios or android using an application called ionic [devapp](https://ionicframework.com/docs/pro/devapp/) that run the application as a native one.
You need to install node, ionic, cv2, and Django REST framework.

In order to run the backend you need to run the script manage.py with this command:

python manage.py runserver ip:8000

where ip is the ip of your server

And then run ionic using the command:

ionic serve -c 

then you need to start ionic devapp and connect to the application.