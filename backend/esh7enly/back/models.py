from django.db import models
from django.contrib.auth.models import User

class UploadedImage(models.Model) :
    image = models.FileField(upload_to='images/photos/', blank=True, null=True)

# class Gift(models.Model):
#     credit_text = models.CharField(max_length=20)
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     submitted_user = models.OneToOneField(User,on_delete=models.CASCADE)

class Card(models.Model):
    credit_text = models.CharField(max_length=20)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    service_provider = models.CharField(max_length=20,null=True)
    date = models.DateField(auto_now=True)

class Gift(models.Model):
    credit_text = models.CharField(max_length=20)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    service_provider = models.CharField(max_length=20,null=True)
    date = models.DateField(auto_now=True)
    submitted_user_email = models.EmailField()


