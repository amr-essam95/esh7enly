from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from back import views

urlpatterns = [
   url(r'^signup/$', views.SignUp.as_view()),
   url(r'^login/$', views.Login.as_view()),
   url(r'^logout/$', views.Logout.as_view()),
   url(r'^test/$', views.Test.as_view()),
   url(r'^upload/$', views.Upload.as_view()),
   url(r'^card/$', views.Card_Handler.as_view()),
   url(r'^delete/$', views.Delete_Card.as_view()),
   url(r'^delete_gift/$', views.Delete_Gift.as_view()),
   url(r'^gift/$', views.Gift_Handler.as_view()),

   # url(r'^children/$', views.getChildren.as_view())
]
