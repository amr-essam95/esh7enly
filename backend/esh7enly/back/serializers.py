from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth.models import User
from drf_extra_fields.fields import Base64ImageField
from back.models import *

class UserSerializer(serializers.ModelSerializer):
	username = serializers.CharField(
		required = True,
		validators=[UniqueValidator(queryset=User.objects.all())]
		)
	password = serializers.CharField(min_length = 8 , write_only=True)

	def create(self, validated_data):
		user = User.objects.create_user(username=validated_data['username'],password=validated_data['password'])
		return user

	class Meta:
		model = User
		fields = ('id','username','password')

class ImgaeUploadSerializer(serializers.ModelSerializer):
	image = Base64ImageField() 
	class Meta:
		model = UploadedImage
		fields = '__all__'

class CardSerializer(serializers.ModelSerializer):
	class Meta:
		model = Card
		fields = ['credit_text','date','service_provider','id']

class GiftSerializer(serializers.ModelSerializer):
	class Meta:
		model = Gift
		fields = ['credit_text','date','service_provider','submitted_user_email','id']

# class GiftSerializer(serializers.ModelSerializer):
# 	# Create a custom method field
#     submitted_user = serializers.SerializerMethodField('_user')

#     # Use this method for the custom field
#     def _user(self, obj):
#         subm_user = self.context['request'].user
#         return subm_user

# 	class Meta:
# 		model = Gift
# 		fields = ('credit_text','submitted_user','user')

# class GiftGet(serializers.ModelSerializer):
# 	class Meta:
# 		model = Gift
# 		fields = ['credit_text']