import cv2,os,shutil
from back.models import *
from .serializers import *
from django.http import Http404
from rest_framework import status
from rest_framework import permissions
from rest_framework.views import APIView
from django.contrib.auth.models import User
from rest_framework.response import Response
from django.contrib.auth import login, authenticate, logout
from .csrf_ignore import CsrfExemptSessionAuthentication,BasicAuthentication
import sys
sys.path.insert(0, 'back/processing/')
import image_processing


def get_credit_text(service_provider):
	folder = "images/photos"
	files = os.listdir(folder)
	image_path = files[0]
	file_path = os.path.join(folder, image_path)
	text = image_processing.process_image(file_path,service_provider)
	delete_images()	
	return  text

def delete_images(folder = "images/photos"):
	for the_file in os.listdir(folder):
		file_path = os.path.join(folder, the_file)
		try:
			if os.path.isfile(file_path):
				os.unlink(file_path)
		except Exception as e:
			print(e)

class SignUp(APIView):
	def post(self, request, format=None):
		serializer = UserSerializer(data = request.data)
		if serializer.is_valid():
			user = serializer.save()
			if user:
				return Response(serializer.data,status=status.HTTP_202_ACCEPTED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Login(APIView):
	permission_classes = [permissions.AllowAny]
	def post(self, request):
		username = request.data.get('username')
		print (username)
		password = request.data.get('password')
		print (password)
		user = authenticate(username=username, password=password)

		###### If the User is valid ######
		if user:
			login(request, user)
			serializer = UserSerializer(user)
			x = self.request.user  ## To send the user status in the state with the response ##
			data = {"valid": True, "errors": ""}
			return Response(data, status=status.HTTP_202_ACCEPTED )
		###### If the User isn't valid ######
		if not user:
			txt = {'valid' : False , 'errors' : "Invalid Username or Password"}
			return Response(txt, status=status.HTTP_401_UNAUTHORIZED)

class Logout(APIView) :
	"""
	Logout the user
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	def get(self, request):
		logout(request)
		return Response({"valid": True}, status=status.HTTP_200_OK)


class Test(APIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	def post(self, request):
		user = self.request.user
		if not self.request.user.is_anonymous : 
			serializer = UserSerializer(user)
			return Response(serializer.data, status=status.HTTP_202_ACCEPTED )
		else : 
			return Response({"valid":False}, status=status.HTTP_400_BAD_REQUEST)

class Upload(APIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	def post(self,request,format=None):
		data = request.data
		print(data.get('service_provider'))
		serializer_class = ImgaeUploadSerializer(data=data)
		if serializer_class.is_valid():
			serializer_class.save()
			credit_text = get_credit_text(data.get('service_provider'))
			print (credit_text)
			return Response({'valid':True,'credit_text':credit_text}, status=status.HTTP_201_CREATED)
		print (serializer_class.errors)
		return Response({'valid':False ,'errors':serializer_class.errors}, status=status.HTTP_400_BAD_REQUEST)

class Card_Handler(APIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	def post(self, request,format=None):
		#get the user object which contains the email and id and other attributes of currently logged in user
		user = self.request.user
		print("service")
		print (self.request.data.get('service_provider'))
		print (self.request.data)
		if not self.request.user.is_anonymous : 
			card = Card.objects.create(user_id=user.id,service_provider=\
			self.request.data.get('service_provider'),credit_text=request.data.get('credit_text'))
			return Response({'valid':True,'errors':'null'}, status=status.HTTP_201_CREATED)
		else : 
			return Response({'valid':False ,'errors':'not logged in'}, status=status.HTTP_400_BAD_REQUEST)
	
	def get(self, request):
		user = self.request.user
		if not self.request.user.is_anonymous : 
			card = Card.objects.filter(user_id=user.id)
			card_serializer = CardSerializer(card,many=True)
			gift = Gift.objects.filter(user_id=user.id)
			gift_serializer = GiftSerializer(gift,many=True)
			return Response({'valid':True,'cards':card_serializer.data,'gifts':gift_serializer.data}\
			, status=status.HTTP_200_OK)
		else : 
			return Response({'valid':False ,'errors':'not logged in'}, status=status.HTTP_400_BAD_REQUEST)

class Delete_Card(APIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	def post(self, request,format=None):
		#get the user object which contains the email and id and other attributes of currently logged in user
		user = self.request.user
		print (self.request.data.get('id'))
		if not self.request.user.is_anonymous : 
			card = Card.objects.get(id = self.request.data.get('id')).delete()
			return Response({'valid':True,'errors':'null'}, status=status.HTTP_201_CREATED)
		else : 
			return Response({'valid':False ,'errors':'not logged in'}, status=status.HTTP_400_BAD_REQUEST)

class Delete_Gift(APIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	def post(self, request,format=None):
		#get the user object which contains the email and id and other attributes of currently logged in user
		user = self.request.user
		print (self.request.data.get('id'))
		if not self.request.user.is_anonymous : 
			card = Gift.objects.get(id = self.request.data.get('id')).delete()
			return Response({'valid':True,'errors':'null'}, status=status.HTTP_201_CREATED)
		else : 
			return Response({'valid':False ,'errors':'not logged in'}, status=status.HTTP_400_BAD_REQUEST)

class Gift_Handler(APIView):
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	def post(self, request,format=None):
		#get the user object which contains the email and id and other attributes of currently logged in user
		user = self.request.user
		print("service")
		print (self.request.data.get('service_provider'))
		print (self.request.data)
		print (self.request.data.get('user_email'))
		user_gift = User.objects.filter(username=self.request.data.get('user_email'))[0]
		if not self.request.user.is_anonymous : 
			gift = Gift.objects.create(user_id=user_gift.id,submitted_user_email=user.username\
			,service_provider=self.request.data.get('service_provider'),credit_text=request.data.get('credit_text'))
			return Response({'valid':True,'errors':'null'}, status=status.HTTP_201_CREATED)
		else : 
			return Response({'valid':False ,'errors':'not logged in'}, status=status.HTTP_400_BAD_REQUEST)