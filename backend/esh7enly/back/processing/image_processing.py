import numpy as np
from PIL import Image
from PIL import ImageFile
Image.LOAD_TRUNCATED_IMAGES = True
ImageFile.LOAD_TRUNCATED_IMAGES = True
import cv2,os,re,pytesseract,math
import detect_characters, rectangles

class possibleRectangle:
    def __init__(self):
        self.imgPlate = None
        self.imgGrayscale = None
        self.imgThresh = None
        self.rrLocationOfPlateInScene = None
        self.strChars = ""

class PossibleChar:

    def __init__(self, _contour):
        self.contour = _contour

        self.boundingRect = cv2.boundingRect(self.contour)

        [intX, intY, intWidth, intHeight] = self.boundingRect

        self.intBoundingRectX = intX
        self.intBoundingRectY = intY
        self.intBoundingRectWidth = intWidth
        self.intBoundingRectHeight = intHeight

        self.intBoundingRectArea = self.intBoundingRectWidth * self.intBoundingRectHeight

        self.intCenterX = (self.intBoundingRectX + self.intBoundingRectX + self.intBoundingRectWidth) / 2
        self.intCenterY = (self.intBoundingRectY + self.intBoundingRectY + self.intBoundingRectHeight) / 2

        self.fltDiagonalSize = math.sqrt((self.intBoundingRectWidth ** 2) + (self.intBoundingRectHeight ** 2))

        self.fltAspectRatio = float(self.intBoundingRectWidth) / float(self.intBoundingRectHeight)

def process_image(image,service_provider = "Vodafone"):
    print ("service: %s" % service_provider)
    
    # KNN training
    blnKNNTrainingSuccessful = detect_characters.KNN()
    # Read image
    imgOriginalScene  = cv2.imread(image)
    
    listOfPossibleRectangles = rectangles.detectRectanges(imgOriginalScene)

    listOfPossibleRectangles = detect_characters.detectCharsInRectangle(listOfPossibleRectangles)

    if len(listOfPossibleRectangles) == 0:
        return -1
    else:
        listOfPossibleRectangles.sort(key = lambda possiblePlate: len(possiblePlate.strChars), reverse = True)
        print("Number of possible Rectangles:",len(listOfPossibleRectangles))
        i = 0
        kernel1 =  cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3)) 
        for rectangle in listOfPossibleRectangles:
            i+=1
            possible_rectangle = rectangle
            if service_provider == "Mobinil" or service_provider == "Etisalat":   
                possible_rectangle.imgThresh = cv2.erode(possible_rectangle.imgThresh,kernel1,iterations = 2)      
                possible_rectangle.imgThresh = cv2.dilate(possible_rectangle.imgThresh,kernel1,iterations = 4)
                resized_image = cv2.resize(possible_rectangle.imgThresh, (0,0), fx=1.5, fy=2)
                # cv2.imshow("imgThresh", possible_rectangle.imgThresh)        
                # cv2.waitKey(0)
            if service_provider == "Vodafone":
                possible_rectangle.imgThresh = cv2.dilate(possible_rectangle.imgThresh,kernel1,iterations = 2)
                resized_image = cv2.resize(possible_rectangle.imgThresh, (0,0), fx=1.5, fy=3)

            # write image to file
            cv2.imwrite("./images/%d.png" % i, resized_image)

            # read image using PIL to use it in tesseract ocr
            im = Image.open('./images/%d.png' % i)

            # use tesseract to get numbers from image
            # This cofiguration of tesseract gets only numbers from 0 to 9
            text = pytesseract.image_to_string(im,lang='eng', config='--psm 10 --eom 3 -c tessedit_char_whitelist=0123456789')
            
            # This regex is intended to get only the 16 number in the credit in vodafone card
            print ("iteration: %s \n\n" % text)

            if service_provider == "Mobinil" or service_provider == "Vodafone":
                r = re.search("([\d]{4}\s[\d]{4}\s[\d]{4}\s[\d]{4})",text)
                if r:
                    # if regex is found this means that we found all the numbers and the process succeded
                    print ("Credit: %s" % r.group(1))
                    return r.group(1).replace(" ","")
            elif service_provider == "Etisalat":
                r = re.search("^([\d]{3}\s*[\d]{3}\s*[\d]{3}\s*[\d]{3}\s*[\d]{3})$",text)
                if r:
                    # if regex is found this means that we found all the numbers and the process succeded
                    print ("Credit: %s" % r.group(1))
                    return r.group(1).replace(" ","")
        return -2