import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { LoginPage} from '../login/login';
import { GeneralProvider } from '../../providers/general/general';
import { Camera, CameraOptions} from '@ionic-native/camera';
import { TabsPage } from '../tabs/tabs';


@IonicPage()
@Component({
  selector: 'page-startup',
  templateUrl: 'startup.html',
})
export class StartupPage {

  myphoto:any;
  imageName:any;
  imgUrl:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public generalProvider:GeneralProvider,public camera:Camera) {
    this.test();
    
  }
  goToSignup(){
    this.navCtrl.push(RegisterPage);
  }
  goToSignIn(){
    this.navCtrl.push(LoginPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad StartupPage');
  }
  ionViewWillEnter() {
    let tabs = document.querySelectorAll('.tabbar');
    if ( tabs !== null ) {
      Object.keys(tabs).map((key) => {
        tabs[ key ].style.transform = 'translateY(56px)';
      });
    } // end if
  }

  ionViewDidLeave() {
    let tabs = document.querySelectorAll('.tabbar');
    if ( tabs !== null ) {
      Object.keys(tabs).map((key) => {
        tabs[ key ].style.transform = 'translateY(0)';
      });
    } // end if
  }
  test(){
    
    this.generalProvider.test().subscribe(res=>{
      console.log(res);
      this.navCtrl.push(TabsPage,{
        
      })

    },
    err => {
      console.log("you are not signed in");
    }
  );
  }
  capture(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.imageName = imageData;
      this.imgUrl = 'data:image/jpeg;base64,' + imageData;
      // this.transferData()
      this.generalProvider.transferData(this.imageName);
    
     }, (err) => {
      //  // Handle error
      });

  }
}
//
// this.camera.getPicture(options).then((imageData) => {
//   this.imageName = imageData;
//   this.imgUrl = 'data:image/jpeg;base64,' + imageData;
//   this.transferData()

//  }, (err) => {
//   // Handle error
//  });
// }
// transferData(){
//  let formData = new FormData();
//  // formData.append('category', 1);
//  // formData.append('status', 'Y');
//  formData.append('image', this.imageName); 
//  this.http.post("http://192.168.1.36:8000/api-imageUpload", formData, {withCredentials: true}).subscribe(res => {
//    let status = res['status'];
//    if(status == 201){
//      var message = "The image was successfully uploaded!";
//      this.showAlert(message);
//    }else{
//      var message = "upload error";
//      this.showAlert(message);
//    }
//  }, (err) => {
//    var message = "Error in uploading file " + err.errors;
//    this.showAlert(message);
//  });
// }
