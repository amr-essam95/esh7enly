import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralProvider } from '../../providers/general/general';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { StartupPage } from '../startup/startup';


/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController,public generalProvider:GeneralProvider,public alertCtrl:AlertController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }
  showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Error!',
      subTitle: message,
      buttons: ['OK']

    });
    
    alert.present();
  }
  logout(){
    this.generalProvider.logout().subscribe(res=>{
      this.navCtrl.push(StartupPage);
    },
    err => {
      this.showAlert("you are not logged in");
    }
  );
  }

}
