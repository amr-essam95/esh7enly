import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { GeneralProvider } from '../../providers/general/general';
import { StartupPage } from '../startup/startup';
import { Camera, CameraOptions} from '@ionic-native/camera';
import { HttpClient } from '@angular/common/http';
import { CallNumber } from '@ionic-native/call-number';
import { Clipboard } from '@ionic-native/clipboard';
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  imageName:any;
  image_url_gallery:any;
  image_gallery:any;
  imgUrl:any
  data:any;
  credit_text:any;
  ip = '192.168.1.6';
  upload_url = "http://"+this.ip+":8000/upload/";
  service_provider:any;
  input:any;
  action:any;
  submitted_user:any;

  constructor(public navCtrl: NavController,public loadingCtrl: LoadingController,private clipboard: Clipboard,private callNumber: CallNumber,public http: HttpClient,public generalProvider:GeneralProvider,public alertCtrl:AlertController,public camera:Camera) {
    this.action = 's';
    this.service_provider = "Vodafone";
    this.input = false;
  }
  showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Alert',
      subTitle: message,
      buttons: ['OK']

    });
    
    alert.present();
  }

  createAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Debugging',
      subTitle: "Hi " + message,
      buttons: [{
        text: "Ok",

      }]
    });
    alert.present();
  }
  logout(){
    this.generalProvider.logout().subscribe(res=>{
      this.navCtrl.push(StartupPage);
    },
    err => {
      this.showAlert("you are not logged in");
    }
  );
  }
  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
        </div>`,
      duration: 5000
    });
  
    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });
  
    loading.present();
  }
  call_number(){

    let number = "";
    if (this.service_provider == 'Vodafone'){
      number = "*858*" + this.credit_text + "#";
    }
    else if(this.service_provider == 'Mobinil'){
      number = "#102*" + this.credit_text + "#";
    }
    else if(this.service_provider == 'Etisalat'){
      number = "*556*" + this.credit_text + "#";
    }
    this.callNumber.callNumber(number, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));

  }
  on_change(){
    if (this.action == 'g'){
      this.input = true;
    }
    else{
      this.input = false;
    }
  }
  process_image(){
    // create new form data that will be sent in the json to the server
    let formData = new FormData();
    // add image and service provider info
    formData.append('image', this.image_gallery);
    formData.append('service_provider',this.service_provider);

    this.http.post(this.upload_url, formData, {withCredentials: true}).subscribe(res => {

      let valid = res['valid'];
      this.credit_text = "";
      console.log(res['credit_text']);

      if(valid == true){
        // if no error occured
        console.log("The image was successfully uploaded!");

        if (parseInt(res['credit_text']) < 0){
          // if the image wasn't correctly processed
          this.credit_text = "Please try again";
        }
        else{
          this.credit_text = res['credit_text'];
          // copy credit_text to clip board
          this.clipboard.copy(this.credit_text);

          if (this.action == 'c'){
            // if action is charge not save the card
            this.showAlert("This Card will be charged\n" + this.credit_text);
            console.log("charging");
            // call number
            this.call_number();
          }
          else if(this.action == 's'){

            this.showAlert("This Card will be saved\n" + this.credit_text);
            this.generalProvider.save(res['credit_text'],this.service_provider).subscribe(res=>{
            console.log("saved");

            },
            err => {
              console.log("error");
            }
          );
          }
        }
        
      }else{
        console.log("upload error");
      }
    }, (err) => {
      console.log("Error in uploading file " + err.errors);
    });

  }
  
  capture(){
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.imageName = imageData;
      this.imgUrl = 'data:image/jpeg;base64,' + imageData;

      let formData = new FormData();
      formData.append('image', this.imageName);
      formData.append('service_provider',this.service_provider);
      console.log(this.service_provider);
      this.http.post(this.upload_url, formData, {withCredentials: true}).subscribe(res => {
        let valid = res['valid'];
        this.credit_text = "";
        console.log(res['credit_text']);

        if(valid == true){
          // console.log("The image was successfully uploaded!");
          if (parseInt(res['credit_text']) < 0){
            this.showAlert("Please Try Again")
          }
          else{
            this.credit_text = res['credit_text'];
            // copy credit_text to clip board
            this.clipboard.copy(this.credit_text);

            if (this.action == 'c'){
              // if action is charge not save the card
              console.log("charging");

              let number = "";
              if (this.service_provider == 'Vodafone'){
                number = "*858*" + this.credit_text + "#";
              }
              else if(this.service_provider == 'Mobinil'){
                number = "#102*" + this.credit_text + "#";
              }
              else if(this.service_provider == 'Etisalat'){
                number = "*556*" + this.credit_text + "#";
              }

              this.callNumber.callNumber(number, true)
              .then(res => console.log('Launched dialer!', res))
              .catch(err => console.log('Error launching dialer', err));
            }
            else if(this.action == 's'){
              this.generalProvider.save(res['credit_text'],this.service_provider).subscribe(res=>{
                console.log("saved");
              },
              err => {
                console.log("error");
              }
            );
            }
            else if(this.action == 'g'){
              console.log("gift handler");
              this.generalProvider.send_gift(res['credit_text'],this.service_provider,this.submitted_user).subscribe(res=>{
                console.log("gift sent");
              },
              err => {
                console.log("error");
              }
            );
            }
          }
          
        }else{
          console.log("upload error");
        }
      }, (err) => {
        console.log("Error in uploading file " + err.errors);
      });
    
     }, (err) => {
      //  // Handle error
      });

  }
  load_from_gallery(){
      const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType : this.camera.PictureSourceType.PHOTOLIBRARY
      }
   
      this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.image_gallery = imageData;
      this.image_url_gallery = 'data:image/jpeg;base64,' + imageData;
      // this.process_image();
      let formData = new FormData();
      formData.append('image', this.image_gallery);
      formData.append('service_provider',this.service_provider);
      console.log(this.service_provider);
      this.http.post(this.upload_url, formData, {withCredentials: true}).subscribe(res => {
        let valid = res['valid'];
        this.credit_text = "";
        console.log(res['credit_text']);

        if(valid == true){
          console.log("The image was successfully uploaded!");
          if (parseInt(res['credit_text']) < 0){
            this.credit_text = "Please try again";
          }
          else{
             this.credit_text = res['credit_text'];
            this.clipboard.copy(this.credit_text);
            if (this.action == 'c'){
              this.showAlert("This Card will be charged\n" + this.credit_text);
              // console.log("charge");
              let number = "";
              if (this.service_provider == 'Vodafone'){
                number = "*858*" + this.credit_text + "#";
              }
              else if(this.service_provider == 'Mobinil'){
                number = "#102*" + this.credit_text + "#";
              }
              else if(this.service_provider == 'Etisalat'){
                number = "*556*" + this.credit_text + "#";
              }
              this.callNumber.callNumber(number, true)
              .then(res => console.log('Launched dialer!', res))
              .catch(err => console.log('Error launching dialer', err));
            }
            else if(this.action == 's'){
              this.showAlert("This Card will be saved\n" + this.credit_text);
              this.generalProvider.save(res['credit_text'],this.service_provider).subscribe(res=>{
                console.log("saved");
              },
              err => {
                console.log("error");
              }
            );
            }
            else if(this.action == 'g'){
              console.log("gift handler");
              this.generalProvider.send_gift(res['credit_text'],this.service_provider,this.submitted_user).subscribe(res=>{
                console.log("gift sent");
                this.generalProvider.getCards().subscribe(res=>{
                  console.log(res['cards']);
                  console.log(res['gifts'])
                },
                err => {
                  console.log("error");
                }
              );
              },
              err => {
                console.log("error");
              }
            );
            }
          }
          
        }else{
          console.log("upload error");
        }
      }, (err) => {
        console.log("Error in uploading file " + err.errors);
      });
      }, (err) => {
      // Handle error
      });
    
  }
  

}
