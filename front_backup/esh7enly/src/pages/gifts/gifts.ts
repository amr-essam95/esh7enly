import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralProvider } from '../../providers/general/general';
import { CallNumber } from '@ionic-native/call-number';
import { Clipboard } from '@ionic-native/clipboard';

/**
 * Generated class for the GiftsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gifts',
  templateUrl: 'gifts.html',
})
export class GiftsPage {
  cards:any;
  my_cards:any;
  gifts:any;
  constructor(public navCtrl: NavController,private callNumber: CallNumber,private clipboard: Clipboard,public generalProvider:GeneralProvider, public navParams: NavParams) {
    this.cards = "My cards";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GiftsPage');
    this.generalProvider.getCards().subscribe(res=>{
      console.log(res['cards']);
      console.log(res['gifts'])
      this.my_cards = res['cards'];
      this.gifts = res['gifts']
    },
    err => {
      console.log("error");
    }
  );
  }
  ionViewWillEnter(){
    console.log('ionViewDidLoad GiftsPage');
    this.generalProvider.getCards().subscribe(res=>{
      console.log(res['cards']);
      console.log(res['gifts'])
      this.my_cards = res['cards'];
      this.gifts = res['gifts']
    },
    err => {
      console.log("error");
    }
  );
  }
  charge(card,credit_text,service_provider,id){
    this.clipboard.copy(credit_text);
    console.log("hi");
    console.log(card);
    let number = "";
    if (service_provider == 'Vodafone'){
      number = "*858*" + credit_text + "#";
    }
    else if(service_provider == 'Mobinil'){
      number = "#102*" + credit_text + "#";
    }
    else if(service_provider == 'Etisalat'){
      number = "*556*" + credit_text + "#";
    }
    this.callNumber.callNumber(number, true)
    .then(res => {
      console.log('Launched dialer!', res);
      this.generalProvider.delete(id).subscribe(res=>{
        console.log("deleted");
        let index = this.my_cards.indexOf(card);
        if(index > -1){
          this.my_cards.splice(index, 1);
          console.log(this.cards);
        }
      },
      err => {
        console.log("error in deletion");
      }
    );
    })
    .catch(err => {
      console.log('Error launching dialer', err);
      this.generalProvider.delete(id).subscribe(res=>{
        console.log("deleted");
        let index = this.my_cards.indexOf(card);
        if(index > -1){
          console.log("hello deleted");
          this.my_cards.splice(index, 1);
          console.log(this.cards);
        }
      },
      err => {
        console.log("error in deletion");
      }
    );
    });


  }
  set(cards,gifts){
    this.cards = cards;
    this.gifts = gifts;
  }
  charge_gift(card,credit_text,service_provider,id){
    this.clipboard.copy(credit_text);
    console.log("hi");
    console.log(card);
    let number = "";
    if (service_provider == 'Vodafone'){
      number = "*858*" + credit_text + "#";
    }
    else if(service_provider == 'Mobinil'){
      number = "#102*" + credit_text + "#";
    }
    else if(service_provider == 'Etisalat'){
      number = "*556*" + credit_text + "#";
    }
    this.callNumber.callNumber(number, true)
    .then(res => {
      console.log('Launched dialer!', res);
      this.generalProvider.delete(id).subscribe(res=>{
        console.log("deleted");
        let index = this.my_cards.indexOf(card);
        if(index > -1){
          this.my_cards.splice(index, 1);
          console.log(this.cards);
        }
      },
      err => {
        console.log("error in deletion");
      }
    );
    })
    .catch(err => {
      console.log('Error launching dialer', err);
      this.generalProvider.delete_gift(id).subscribe(res=>{
        console.log("deleted");
        let index = this.gifts.indexOf(card);
        if(index > -1){
          console.log("hello deleted");
          this.gifts.splice(index, 1);
          console.log(this.cards);
        }
      },
      err => {
        console.log("error in deletion");
      }
    );
    });


  }

}
