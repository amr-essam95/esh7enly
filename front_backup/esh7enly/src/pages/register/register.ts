import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralProvider } from '../../providers/general/general';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { LoginPage } from '../login/login';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  data ;
  messages:any;
  message_body:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public generalProvider:GeneralProvider,public alertCtrl:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  ionViewWillEnter() {
    let tabs = document.querySelectorAll('.tabbar');
    if ( tabs !== null ) {
      Object.keys(tabs).map((key) => {
        tabs[ key ].style.transform = 'translateY(56px)';
      });
    } // end if
  }

  ionViewDidLeave() {
    let tabs = document.querySelectorAll('.tabbar');
    if ( tabs !== null ) {
      Object.keys(tabs).map((key) => {
        tabs[ key ].style.transform = 'translateY(0)';
      });
    } // end if
  }
  register(data){
    this.data = data;
    this.generalProvider.register(JSON.stringify(data.value)).subscribe(res=>{
      this.createAlert("Congrats your account was created!");

    },
    err => {
      this.messages = {
        "username":err.error.username,
        "password":err.error.password
      }
      if (this.messages.username && this.messages.password){
        
      }
      else{
        if (this.messages.username){
          this.messages = {
            "password":"No errors \n",
            "username":err.error.username+"\\\\",
          }
        }
        else{
          this.messages = {
            "email":"No errors \n. \\\\",
            "password":err.error.password,
          }
        }

      }
      this.showAlert(this.messages);
    

    }
  );
  }
  showAlert(message) {
    
       
    let alert = this.alertCtrl.create({
      title: 'Error!',
      
      message:"Email : "+message.username+"     "+ "Password : "+message.password,
  
      buttons: ['OK']

    });
    alert.present();
  }

  createAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Congrats!',
      subTitle: message,
      buttons: [{
        text: "Ok",
        handler :()=>{
          this.navCtrl.push(LoginPage,{
            username : this.data.value.username,
            password : this.data.value.password
          })
        }
      }]
    });
    alert.present();
  }

}
