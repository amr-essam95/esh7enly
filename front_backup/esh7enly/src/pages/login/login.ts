import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { GeneralProvider } from '../../providers/general/general';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  data;
  constructor(public navCtrl: NavController, public navParams: NavParams,public generalProvider:GeneralProvider,public alertCtrl:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  ionViewWillEnter() {
    let tabs = document.querySelectorAll('.tabbar');
    if ( tabs !== null ) {
      Object.keys(tabs).map((key) => {
        tabs[ key ].style.transform = 'translateY(56px)';
      });
    } // end if
  }

  ionViewDidLeave() {
    let tabs = document.querySelectorAll('.tabbar');
    if ( tabs !== null ) {
      Object.keys(tabs).map((key) => {
        tabs[ key ].style.transform = 'translateY(0)';
      });
    } // end if
  }
  login(data){
    this.data = data;
    this.generalProvider.login(JSON.stringify(data.value)).subscribe(res=>{
      this.navCtrl.push(TabsPage,{
        username : this.data.value.username,
        password : this.data.value.password
      })

    },
    err => {
      this.showAlert("Wrong email or password");
    }
  );
  }
  showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Errors',
      subTitle: message,
      buttons: ['OK']

    });
    alert.present();
  }
}
