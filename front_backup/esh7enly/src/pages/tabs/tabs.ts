import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import {SettingsPage} from '../settings/settings'
import {GiftsPage} from '../gifts/gifts'

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = GiftsPage;
  tab3Root = SettingsPage;

  constructor() {

  }
}
