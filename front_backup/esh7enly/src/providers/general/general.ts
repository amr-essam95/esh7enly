import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

// import { HttpClientModule } from '@angular/common/http'; 
// import { HttpModule } from '@angular/http';
@Injectable()
export class GeneralProvider {

  constructor(public http: HttpClient) {
    console.log('Hello General  Provider');
  }
  headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
  headers2 = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});

  ip = '192.168.1.6';
  register_url = "http://"+this.ip+":8000/signup/";
  login_url = "http://"+this.ip+":8000/login/";
  test_url = "http://"+this.ip+":8000/test/";
  logout_url = "http://"+this.ip+":8000/logout/";
  upload_url = "http://"+this.ip+":8000/upload/";
  card_url = "http://"+this.ip+":8000/card/";
  delete_url = "http://"+this.ip+":8000/delete/";
  delete_gift_url = "http://"+this.ip+":8000/delete_gift/";
  gift_url = "http://"+this.ip+":8000/gift/";

  register(data){
    console.log(data);
    // console.log(this.register_url);
    return this.http.post(this.register_url,data,{headers:this.headers});
  }
  login(data){
    console.log(data);
    console.log(this.login_url);
    return this.http.post(this.login_url,data,{withCredentials: true,headers:this.headers});
  }
  test(){
    return this.http.post(this.test_url,"",{withCredentials: true,
      headers:this.headers});
  }
  logout(){
    return this.http.get(this.logout_url,{withCredentials: true,
      headers:this.headers});
  }
  upload(data){
    return this.http.post(this.upload_url,"",{withCredentials: true,
      headers:this.headers2});

  }
  save(credit_text,service_provider){
    let submitted_data = JSON.stringify({"credit_text":credit_text,"service_provider":service_provider});
    return this.http.post(this.card_url,submitted_data,{withCredentials: true,
      headers:this.headers2});
  }
  delete(id){
    return this.http.post(this.delete_url,JSON.stringify({"id":id}),{withCredentials: true,
      headers:this.headers2});
  }
  delete_gift(id){
    return this.http.post(this.delete_gift_url,JSON.stringify({"id":id}),{withCredentials: true,
      headers:this.headers2});
  }
  getCards(){
    return this.http.get(this.card_url,{withCredentials: true,
      headers:this.headers});
  }
  send_gift(credit_text,service_provider,submitted_user){
    let submitted_data = JSON.stringify({"credit_text":credit_text,"service_provider":service_provider,"user_email":submitted_user});
    return this.http.post(this.gift_url,submitted_data,{withCredentials: true,
      headers:this.headers2});
  }
  transferData(imageName){
    let formData = new FormData();
    // formData.append('category', 1);
    // formData.append('status', 'Y');
    formData.append('image', imageName); 
    return this.http.post(this.upload_url, formData, {withCredentials: true}).subscribe(res => {
      let status = res['status'];
      if(status == 201){
        var message = "The image was successfully uploaded!";
        // this.showAlert(message);
      }else{
        var message = "upload error";
        // this.showAlert(message);
      }
    }, (err) => {
      var message = "Error in uploading file " + err.errors;
      // this.showAlert(message);
    });
  }

}
