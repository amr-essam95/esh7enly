import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { StartupPage} from '../pages/startup/startup';
import { LoginPage} from '../pages/login/login';
import { RegisterPage} from '../pages/register/register';
import { TabsPage } from '../pages/tabs/tabs';
import {SettingsPage} from '../pages/settings/settings'
import {GiftsPage} from '../pages/gifts/gifts'

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GeneralProvider } from '../providers/general/general';
import { Camera } from '@ionic-native/camera';
import { CallNumber } from '@ionic-native/call-number';
import { Clipboard } from '@ionic-native/clipboard';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    StartupPage,
    LoginPage,
    GiftsPage,
    SettingsPage,
    RegisterPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    StartupPage,
    GiftsPage,
    SettingsPage,
    LoginPage,
    RegisterPage
  ],
  providers: [
    CallNumber,
    Clipboard,
    StatusBar,
    SplashScreen,Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GeneralProvider,
  ]
})
export class AppModule {}
